FROM python:3.6-alpine

# Set env variables used in this Dockerfile (add a unique prefix, such as DOCKYARD)
# Local directory with project source
# Directory in container for all project files
ENV APP_HOME=/srv/superlists/
# Directory in container for project source files

# Create application subdirectories
RUN mkdir $APP_HOME
VOLUME ["$APP_HOME/media/", "$APP_HOME/logs/"]

# Copy application source code to SRCDIR
COPY . $APP_HOME

WORKDIR $APP_HOME

# Install Python dependencies
RUN pip3 install -r requirements.txt

# Port to expose
EXPOSE 8000

# Copy entrypoint script into the image
COPY ./docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

